import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import App from "./App";
import { QueryClient, QueryClientProvider } from "react-query";

import { i18n } from "@lingui/core";
import { I18nProvider } from "@lingui/react";
import  messagesEn from "./locales/en/messages.json";
import  messagesEs from "./locales/es/messages.json";

i18n.load({
  en: messagesEn,
  es: messagesEs
});

i18n.loadLocaleData({
  en: messagesEn,
  es: messagesEs
});

i18n.activate("es");

const client = new QueryClient();
sessionStorage.clear();

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <I18nProvider i18n={i18n}>
        <QueryClientProvider client={client}>
          <App />
        </QueryClientProvider>      
      </I18nProvider>
    </Router>
  </React.StrictMode>,
  document.getElementById("root")
);
