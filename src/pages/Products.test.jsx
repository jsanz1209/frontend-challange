import { i18n } from "@lingui/core";
import { I18nProvider } from "@lingui/react";
import { render, screen, waitFor } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import { BrowserRouter as Router } from "react-router-dom";
import { Products as ProductsAPI } from "../API/Products";
import { messages } from "../locales/en/messages.json";
import { messages as esMessages } from "../locales/es/messages.json";
import {
  errorResponse,
  successResponse
} from "../test/__mocks__/fetchProducts";
import Products from "./Products";

import { en, es } from 'make-plural/plurals'

i18n.load({
  en: messages,
  es: esMessages,
});

i18n.loadLocaleData({
  en: { plurals: en },
  es: { plurals: es }
});

jest.spyOn(i18n, 'activate').mockReturnValueOnce({
  t: (val) => 'Products',
  i18n: { exists: () => true }
}).mockReturnValue({
  t: (val) => 'Productos',
  i18n: { exists: () => true }
});

const TestingProvider = ({ children }) => (
  <Router>
    <I18nProvider i18n={i18n} forceRenderOnLocaleChange={false}>
      {children}
    </I18nProvider>
  </Router>
);

jest.mock('../components/LanguageSelector', () => {
  const ComponentToMock = () => <div />;
  return ComponentToMock;
});


beforeEach(() => {
  fetch.resetMocks();
  sessionStorage.clear();
});

it("Fetch results getting successfull response", async () => {
  fetch.mockResponseOnce(JSON.stringify(successResponse));
  const response = await ProductsAPI.getProducts({
    tax_filter: [
      "es_general_21",
      "es_reduced_10",
      "es_super-reduced_4",
      "fr_general_20",
      "fr_reduced_5.5",
    ],
    title_filter: "",
    order_by: "",
    order: "",
    page: 1,
    per_page: 5,
  });
  const json = await response.json();
  expect(json).toEqual(successResponse);
  expect(fetch).toHaveBeenCalledTimes(1);
});

it("Fetch results getting error response", async () => {
  fetch.mockResponseOnce(JSON.stringify(errorResponse));
  const response = await ProductsAPI.getProducts({
    tax_filter: [
      "es_general_21",
      "es_reduced_10",
      "es_super-reduced_4",
      "fr_general_20",
      "fr_reduced_5.5",
    ],
    title_filter: "",
    order_by: "",
    order: "",
    per_page: 5,
  });

  const json = await response.json();
  expect(json).toEqual(errorResponse);
  expect(fetch).toHaveBeenCalledTimes(1);
});

it("Throw error when exception is procued", async () => {
  try {
    fetch.mockReject(() => Promise.reject("API is down"));
  } catch (error) {
    expect(error).toEqual("APId is down");
  }
});

it("Renders table when response is sucessfully", async () => {
  fetch.mockResponseOnce(JSON.stringify(successResponse));
  expect(sessionStorage.getItem('cacheParams')).toBeNull();
  const { getByTestId } = render(
    <TestingProvider>
      <Products />
    </TestingProvider>
  );
  await waitFor(() => getByTestId("main-container"));
  const table = screen.getAllByRole("table")[0];
  expect(table).toBeDefined();
  const totalResults = screen.queryAllByText("953");
  expect(totalResults).toHaveLength(2);
  const rows = screen.getAllByRole("row");
  expect(rows).toHaveLength(6);
  expect(sessionStorage.getItem('cacheParams')).not.toBeNull();
});

it("No renders table when response is wrong", async () => {
  act(() => {
    i18n.activate('en');
  });
  fetch.mockResponseOnce(JSON.stringify(errorResponse));
  expect(sessionStorage.getItem('cacheParams')).toBeNull();
  const { getByTestId } = render(
    <TestingProvider>
      <Products />
    </TestingProvider>
  );
  await waitFor(() => getByTestId("main-container"));
  const table = screen.getAllByRole("table")[0];
  expect(table).toBeDefined();
  let rows = screen.queryAllByDisplayValue("953");
  expect(rows).toHaveLength(0);
  expect(sessionStorage.getItem('cacheParams')).toBeNull();
  // Translations
  let productText = screen.getByText('Products');
  expect(productText).toBeDefined();
  act(() => {
    i18n.activate('es');
  });
  productText = screen.getByText('Products');
  expect(productText).toBeDefined();
});
