import { t, Trans } from "@lingui/macro";
import React, { useCallback, useEffect, useState } from "react";
import { Products as ProductsAPI } from "../API/Products";
import FilterButton from "../components/DropdownFilter";
import Loading from "../components/Loading";
import PaginationNumeric from "../components/PaginationNumeric";
import SearchForm from "../partials/actions/SearchForm";
import Header from "../partials/Header";
import ProductsTable from "../partials/products/ProductsTable";
import Sidebar from "../partials/Sidebar";
import Transition from "../utils/Transition";

function Products() {
  const cacheParams = sessionStorage.getItem("cacheParams")
    ? JSON.parse(sessionStorage.getItem("cacheParams"))
    : undefined;
  const [list, setList] = useState(cacheParams?.list ?? []);
  const [pagination, setPagination] = useState(cacheParams?.pagination ?? null);
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [titleFilter, setTitleFilter] = useState(cacheParams?.titleFilter);
  const [taxFilter, setTaxFilter] = useState(
    cacheParams
      ? cacheParams.taxFilter
      : [
          "es_general_21",
          "es_reduced_10",
          "es_super-reduced_4",
          "fr_general_20",
          "fr_reduced_5.5",
        ]
  );
  const [page, setPage] = useState(cacheParams ? cacheParams.page : 1);
  const [orderBy, setOrderBy] = useState(cacheParams?.orderBy);
  const [order, setOrder] = useState(cacheParams?.order);

  const fetchProducts = useCallback(() => {
    if (
      titleFilter !== cacheParams?.titleFilter ||
      taxFilter !== cacheParams?.taxFilter ||
      page !== cacheParams?.page ||
      orderBy !== cacheParams?.orderBy ||
      order !== cacheParams?.order
    ) {
      ProductsAPI.getProducts({
        tax_filter: taxFilter,
        title_filter: titleFilter,
        order_by: orderBy,
        order: order,
        page: page,
        per_page: 5,
      })
        .then((response) => {
          setIsLoading(false);
          if (response.status >= 400) {
            throw new Error("Error fetching data");
          } else {
            return response.json();
          }
        })
        .then((res) => {
          if (!res.errors) {
            const { results, pagination } =
              ProductsAPI.transformDataProducts(res);
            setPagination(pagination);
            setList(results);
            sessionStorage.setItem(
              "cacheParams",
              JSON.stringify({
                list: results,
                pagination,
                titleFilter,
                taxFilter,
                page,
                orderBy,
                order,
              })
            );
          }
        });
    } else {
      setIsLoading(false);
    }
  }, [titleFilter, taxFilter, page, orderBy, order]);

  useEffect(() => {
    fetchProducts();
  }, [fetchProducts, titleFilter, taxFilter, page, orderBy, order]);

  const handleChangeSearch = (event) => {
    const promise = setTimeout(() => {
      setTitleFilter(event.target.value);
      setPage(1);
      clearTimeout(promise);
    }, 700);
  };

  const handleChangePage = (page) => {
    setPage(page);
  };

  const handleNextPage = () => {
    setPage((prevState) => prevState + 1);
  };

  const handlePreviousPage = () => {
    setPage((prevState) => prevState - 1);
  };

  const handleOrderDesc = (field) => {
    if (orderBy === field && order === "desc") {
      setOrder("");
      setOrderBy("");
    } else {
      setOrderBy(field);
      setOrder("desc");
    }
    setPage(1);
  };

  const handleOrderAsc = (field) => {
    if (orderBy === field && order === "asc") {
      setOrder("");
      setOrderBy("");
    } else {
      setOrderBy(field);
      setOrder("asc");
    }
    setPage(1);
  };

  const handleChangeTaxes = (taxes) => {
    setTaxFilter(taxes);
    setPage(1);
  };

  return (
    <div className="flex h-screen overflow-hidden" data-testid="main-container">
      {/* Sidebar */}
      <Sidebar sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
      {/* Content area */}
      <div className="relative flex flex-col flex-1 overflow-y-auto overflow-x-hidden">
        {/*  Site header */}
        <Header sidebarOpen={sidebarOpen} setSidebarOpen={setSidebarOpen} />
        <main>
          <Transition
            show={!isLoading}
            tag="div"
            enter="transition ease-out duration-200 transform"
            enterStart="opacity-0 -translate-y-2"
            enterEnd="opacity-100 translate-y-0"
            leave="transition ease-out duration-200"
            leaveStart="opacity-100"
            leaveEnd="opacity-0"
          >
            <div className="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">
              {/* Page header */}
              <div className="sm:flex sm:justify-between sm:items-center mb-5">
                {/* Left: Title */}
                <div className="mb-4 sm:mb-0">
                  <h1 className="text-2xl md:text-3xl text-slate-800 font-bold">
                    <Trans>Catalog</Trans>
                  </h1>
                </div>
                {/* Right: Actions */}
                <div className="grid grid-flow-col sm:auto-cols-max justify-start sm:justify-end gap-2">
                  {/* Search form */}
                  <SearchForm
                    titleFilter={titleFilter}
                    placeholder={t`Search...`}
                    handleChangeSearch={handleChangeSearch}
                  />
                  {/* Filter button */}
                  <FilterButton
                    align="right"
                    taxFilter={taxFilter}
                    handleChangeTaxes={handleChangeTaxes}
                  />
                </div>
              </div>
              {/* Table */}
              <ProductsTable
                list={list}
                pagination={pagination}
                order={order}
                orderBy={orderBy}
                handleOrderAsc={handleOrderAsc}
                handleOrderDesc={handleOrderDesc}
              />
              {/* Pagination */}
              <div className="mt-8">
                {pagination && (
                  <PaginationNumeric
                    pagination={pagination}
                    handleChangePage={handleChangePage}
                    handleNextPage={handleNextPage}
                    handlePreviousPage={handlePreviousPage}
                  />
                )}
              </div>
            </div>
          </Transition>
          {isLoading && <Loading />}
        </main>
      </div>
    </div>
  );
}

export default Products;
