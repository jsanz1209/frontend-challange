export const successResponse = {
    "data": {
        "fetchProducts": {
            "results": [
                {
                    "id": "2",
                    "title": "Lightweight Wooden Coat",
                    "price": 69.43,
                    "tax": "es_general_21",
                    "stock": 21
                },
                {
                    "id": "3",
                    "title": "Lightweight Wooden Bench",
                    "price": 9.45,
                    "tax": "es_super-reduced_4",
                    "stock": 19
                },
                {
                    "id": "4",
                    "title": "Aerodynamic Aluminum Pants",
                    "price": 81.76,
                    "tax": "es_reduced_10",
                    "stock": 19
                },
                {
                    "id": "5",
                    "title": "Intelligent Paper Car",
                    "price": 53.85,
                    "tax": "es_general_21",
                    "stock": 44
                },
                {
                    "id": "6",
                    "title": "Incredible Wool Shoes",
                    "price": 77.32,
                    "tax": "fr_general_20",
                    "stock": 19
                }
            ],
            "pagination": {
                "totalResults": 953,
                "limitValue": 5,
                "totalPages": 191,
                "currentPage": 1,
                "nextPage": 2,
                "prevPage": null,
                "firstPage": true,
                "lastPage": false,
                "outOfRange": false
            }
        }
    }
};

export const errorResponse = {
    "errors": [
        { "message": "Variable $page of type Int! was provided invalid value", "locations": [{ "line": 6, "column": 9 }], "extensions": { "value": null, "problems": [{ "path": [], "explanation": "Expected value to not be null" }] } }]
};