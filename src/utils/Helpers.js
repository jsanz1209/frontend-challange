import { t } from "@lingui/macro"

export class Helpers {

    static taxes = [
        {
            id: 'es_general_21',
            label: t`ES General - 21%`
        },
        {
            id: 'es_reduced_10',
            label: t`ES Reduced - 10%`
        },
        {
            id: 'es_super-reduced_4',
            label: t`ES Super Reduced - 4%`
        },
        {
            id: 'fr_general_20',
            label: t`FR General - 20%`
        },
        {
            id: 'fr_reduced_5.5',
            label: t`FR Reduced - 5.5%`
        }
    ];

    static getLabelTaxt(tax) {
        const taxItem = Helpers.taxes.find(itemTax => itemTax.id === tax);
        if (!taxItem) {
            return t`Unknown`;
        }
        return taxItem.label;
    }

    static formatPrice(price) {
        return `${price.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 })} €`;
    }
}