import React from 'react';

import ENImage from '../images/uk.png';
import ESImage from '../images/es.png';

import { i18n } from "@lingui/core";

function LanguageSelector() {

    const handleChangeLang = (lang) => {
        i18n.activate(lang);
    }

    return <div className='flex'>
        <button className='flex items-center' onClick={handleChangeLang.bind(this, 'en')}>
            <img src={ENImage} alt='English' width='40' height='20' className='h-full'/>
        </button>
        <button className='flex items-center'>
            <img className='ml-2 h-full' src={ESImage} alt='Español' width='40' height='20' onClick={handleChangeLang.bind(this, 'es')}/>
        </button>
    </div>
}

export default LanguageSelector;