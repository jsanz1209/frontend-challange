import { ArrowNarrowDownIcon, ArrowNarrowUpIcon } from '@heroicons/react/solid'

function OrderArrowButtons({orderBy, orderBySelected, orderSelected, handleOrderDesc, handleOrderAsc}) {
    return <>
        <button onClick={handleOrderDesc.bind(this, orderBy)}>
            <ArrowNarrowDownIcon className={`h-3 w-3 ${orderBy === orderBySelected && orderSelected === 'desc' ? 'text-theme-500' : 'text-gray-500'} `} />
        </button>
        <button onClick={handleOrderAsc.bind(this, orderBy)}>
            <ArrowNarrowUpIcon className={`h-3 w-3 ${orderBy === orderBySelected && orderSelected === 'asc' ? 'text-theme-500' : 'text-gray-500'} `} />
        </button>
    </>
}

export default OrderArrowButtons;