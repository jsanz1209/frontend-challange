import React, { useState, useRef, useEffect } from 'react';
import Transition from '../utils/Transition';
import { Trans } from '@lingui/macro'
import { Helpers } from '../utils/Helpers';

function DropdownFilter({
  align, taxFilter, handleChangeTaxes,
}) {

  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [taxFilterSelected, setTaxFilterSelected] = useState(taxFilter);
  const trigger = useRef(null);
  const dropdown = useRef(null);

  const handleAppyTaxes = () => {
    setDropdownOpen(false);
    handleChangeTaxes(taxFilterSelected);
  }

  const handleTaxChange = (event) => {
    const id = event.target.id;
    if (event.target.checked) {
      setTaxFilterSelected((prevState) => {
        return [...prevState, id];
      });
    } else {
      setTaxFilterSelected((prevState) => {
        const index = prevState.findIndex(taxId => taxId === id);
        if (index !== -1) {
          prevState.splice(index, 1);
        }
        return [...prevState];
      });
    }
  }

  const handleClearTaxes = () => {
    setTaxFilterSelected([])
    handleChangeTaxes([]);	
  }

  // close on click outside
  useEffect(() => {
    const clickHandler = ({ target }) => {
      if (!dropdown.current) return;
      if (!dropdownOpen || dropdown.current.contains(target) || trigger.current.contains(target)) return;
      setDropdownOpen(false);
    };
    document.addEventListener('click', clickHandler);
    return () => document.removeEventListener('click', clickHandler);
  });

  // close if the esc key is pressed
  useEffect(() => {
    const keyHandler = ({ keyCode }) => {
      if (!dropdownOpen || keyCode !== 27) return;
      setDropdownOpen(false);
    };
    document.addEventListener('keydown', keyHandler);
    return () => document.removeEventListener('keydown', keyHandler);
  });

  return (
    <div className="relative inline-flex">
      <button
        ref={trigger}
        className="btn bg-white border-slate-200 hover:border-slate-300 text-slate-500 hover:text-slate-600"
        aria-haspopup="true"
        onClick={() => setDropdownOpen(!dropdownOpen)}
        aria-expanded={dropdownOpen}
      >
        <span className="sr-only">Filter</span><wbr />
        <svg className="w-4 h-4 fill-current" viewBox="0 0 16 16">
          <path d="M9 15H7a1 1 0 010-2h2a1 1 0 010 2zM11 11H5a1 1 0 010-2h6a1 1 0 010 2zM13 7H3a1 1 0 010-2h10a1 1 0 010 2zM15 3H1a1 1 0 010-2h14a1 1 0 010 2z" />
        </svg>
      </button>
      <Transition
        show={dropdownOpen}
        tag="div"
        className={`origin-top-right z-10 absolute top-full min-w-56 bg-white border border-slate-200 pt-1.5 rounded shadow-lg overflow-hidden mt-1 ${align === 'right' ? 'right-0' : 'left-0'}`}
        enter="transition ease-out duration-200 transform"
        enterStart="opacity-0 -translate-y-2"
        enterEnd="opacity-100 translate-y-0"
        leave="transition ease-out duration-200"
        leaveStart="opacity-100"
        leaveEnd="opacity-0"
      >
        <div ref={dropdown}>
          <div className="text-xs font-semibold text-slate-400 uppercase pt-1.5 pb-2 px-4"><Trans>Taxes</Trans></div>
          <ul className="mb-4">
            {
              Helpers.taxes.map(tax => {
                return <li className="py-1 px-3" key={tax.id}>
                  <label className="flex items-center">
                    <input type="checkbox"
                      id={tax.id} className="form-checkbox"
                      checked={taxFilterSelected.includes(tax.id)}
                      onChange={handleTaxChange} />
                    <span className="text-sm font-medium ml-2">{tax.label}</span>
                  </label>
                </li>
              })
            }
          </ul>
          <div className="py-2 px-3 border-t border-slate-200 bg-slate-50">
            <ul className="flex items-center justify-between">
              <li>
                <button className="btn-xs bg-white border-slate-200 hover:border-slate-300 text-slate-500 hover:text-slate-600"
                  onClick={handleClearTaxes}><Trans>Clear</Trans></button>
              </li>
              <li>
                <button className="btn-xs bg-theme-500 hover:bg-theme-600 text-white"
                  onClick={handleAppyTaxes}
                  onBlur={() => setDropdownOpen(false)}><Trans>Apply</Trans></button>
              </li>
            </ul>
          </div>
        </div>
      </Transition>
    </div>
  );
}

export default DropdownFilter;
