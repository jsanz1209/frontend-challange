import React from 'react';
import { Trans } from '@lingui/macro'

function PaginationNumeric({ pagination, handleChangePage, handleNextPage, handlePreviousPage }) {

  const { firstPage, lastPage, currentPage, totalResults, limitValue, totalPages } = pagination;

  const getInitialValueFromCurrentPage = () => {
    const value = (limitValue * (currentPage - 1)) + 1;
    return value;
  }

  const getClassesArrowBtn = (isDisabled) => {
    const hoverClasses = 'hover:bg-theme-500 hover:text-white';
    return `inline-flex items-center justify-center rounded leading-5 px-2.5 py-2 bg-white border border-slate-200 text-slate-600 shadow-sm ${!isDisabled ? hoverClasses : ''}`
  }

  const getRangesPagination = () => {
    return (currentPage <= totalPages - limitValue ? currentPage : totalPages - (limitValue - 1))
  }

  const getHTMLTotalResults = () => {
    if (totalResults === 0) {
      return 0;
    } else {
      return (<>
        {getInitialValueFromCurrentPage()} a <span className="font-medium text-slate-600">{(limitValue * currentPage)}</span>
      </>);
    }
  }

  return (
    <>
      <div className="flex justify-center">
        <nav className="flex overflow-hidden" role="navigation" aria-label="Navigation">
          <div className="mr-2">
            <button disabled={firstPage || totalResults === 0}
              onClick={handlePreviousPage}
              className={getClassesArrowBtn(firstPage || totalResults === 0)}>
              <span className="sr-only">Previous</span><wbr />
              <svg className="h-4 w-4 fill-current" viewBox="0 0 16 16">
                <path d="M9.4 13.4l1.4-1.4-4-4 4-4-1.4-1.4L4 8z" />
              </svg>
            </button>
          </div>
          {totalResults > 0 && <ul className="inline-flex text-sm font-medium -space-x-px shadow-sm overflow-hidden">
            {
              Array.from({ length: limitValue }, (_, i) => i + getRangesPagination())
                .filter(page => page > 0 && page <= totalPages)
                .map(page => {
                  const itemClasses = currentPage === page ? 'rounded-l border-slate-200 text-theme-500 cursor-auto' : 'cursor-pointer hover:bg-theme-500 border border-slate-200 text-slate-600 hover:text-white';
                  return <li key={page}>
                    <button className={`inline-flex items-center justify-center leading-5 px-3.5 py-2 bg-white border ${itemClasses}`} onClick={handleChangePage.bind(this, page)}>{page}</button>
                  </li>
                })
            }
          </ul>}
          <div className="ml-2">
            <button disabled={lastPage || totalResults === 0}
              onClick={handleNextPage}
              className={getClassesArrowBtn(lastPage || totalResults === 0)}>
              <span className="sr-only">Next</span><wbr />
              <svg className="h-4 w-4 fill-current" viewBox="0 0 16 16">
                <path d="M6.6 13.4L5.2 12l4-4-4-4 1.4-1.4L12 8z" />
              </svg>
            </button>
          </div>
        </nav>
      </div>
      <div className="flex justify-center">
        <div className="text-sm text-slate-500 text-center sm:text-left py-2">
          <span className="font-medium text-slate-600"></span> {getHTMLTotalResults()}  de <span className="font-medium text-slate-600">{totalResults}</span> <Trans>results</Trans>
        </div>
      </div>
    </>
  );
}

export default PaginationNumeric;
