import { Settings } from './Settings';
import { Helpers } from '../utils/Helpers';

export class Products {

    static FETCH_PRODUCTS_QUERY = `query FetchProducts(
        $tax_filter: [String!],
        $title_filter: String,
        $order_by: String,
        $order: String,
        $page: Int!,
        $per_page: Int!){         
            fetchProducts {
                results(taxFilter: $tax_filter, titleFilter: $title_filter, orderBy: $order_by, order: $order, page: $page, perPage: $per_page) {
                id
                title
                price
                tax
                stock
            }        
            pagination(taxFilter: $tax_filter, titleFilter: $title_filter, orderBy: $order_by, order: $order, page: $page, perPage: $per_page) {
                totalResults
                limitValue
                totalPages
                currentPage
                nextPage
                prevPage
                firstPage
                lastPage
                outOfRange
            }
        }
    }`;

    static getProducts({ tax_filter, title_filter, order_by, order, page, per_page }) {
        return fetch(Settings.endpoint, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                query: Products.FETCH_PRODUCTS_QUERY,
                variables: {
                    tax_filter,
                    title_filter,
                    order_by,
                    order,
                    page,
                    per_page
                }
            })
        })
    }

    static transformDataProducts(res) {
        const results = res.data.fetchProducts.results.map(item => {
            return {
                ...item,
                taxLabel: Helpers.getLabelTaxt(item.tax),
                formattedPrice: Helpers.formatPrice(item.price)
            };
        });
        const pagination = res.data.fetchProducts.pagination;
        return { results, pagination };
    }
}