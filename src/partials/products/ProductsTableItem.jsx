import React from 'react';
import { t } from "@lingui/macro";

function ProductsTableItem(props) {

  const { id, title, formattedPrice, taxLabel, stock } = props;

  return (
    <tr>
      <td className="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
        <div className="font-medium text-sky-500">{id}</div>
      </td>
      <td className="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
        <div className="font-light text-black">{title}</div>
      </td>
      <td className="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
        <div className={`font-semibold text-black-500`}>{formattedPrice}</div>
      </td>
      <td className="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
        <div className={`inline-flex font-medium rounded-full text-center px-2.5 py-0.5 bg-theme-100 text-theme-600`}>{taxLabel}</div>
      </td >
      <td className="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
        <div className={`inline-flex font-medium rounded-full text-center px-2.5 py-0.5 ${stock > 0 ? 'bg-green-100 text-green-600' : 'bg-red-100 text-red-600'}`}>{stock > 0 ? stock : t`Empty`}</div>
      </td >
    </tr>
  );
}

export default ProductsTableItem;
