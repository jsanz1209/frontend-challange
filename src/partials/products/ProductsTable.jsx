import React, { useState } from 'react';
import Products from './ProductsTableItem';
import { Trans } from '@lingui/macro'
import OrderArrowButtons from '../../components/OrderArrowButtons';

function ProductsTable({ list, pagination, orderBy, order, handleOrderDesc, handleOrderAsc }) {

  return (
    <div className="bg-white shadow-lg rounded-sm border border-slate-200 relative">
      <header className="px-5 py-4">
        <h2 className="font-semibold text-slate-800"><Trans>Products</Trans> <span className="text-slate-400 font-medium">{pagination?.totalResults}</span></h2>
      </header>
      <div>
        {/* Table */}
        <div className="overflow-x-auto">
          <table className="table-auto w-full">
            {/* Table header */}
            <thead className="text-xs font-semibold uppercase text-slate-500 bg-slate-50 border-t border-b border-slate-200">
              <tr>
                <th className="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                  <div className='flex items-center'>
                    <div className="font-semibold text-left"><Trans>SKU</Trans></div>
                    <OrderArrowButtons orderBy='id' orderBySelected={orderBy} orderSelected={order} handleOrderDesc={handleOrderDesc} handleOrderAsc={handleOrderAsc} />
                  </div>
                </th>
                <th className="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                  <div className='flex items-center'>
                    <div className="font-semibold text-left"><Trans>Item</Trans></div>
                    <OrderArrowButtons orderBy='title' orderBySelected={orderBy} orderSelected={order} handleOrderDesc={handleOrderDesc} handleOrderAsc={handleOrderAsc} />
                  </div>
                </th>
                <th className="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                  <div className='flex items-center'>
                    <div className="font-semibold text-left"><Trans>Price</Trans></div>
                    <OrderArrowButtons orderBy='price' orderBySelected={orderBy} orderSelected={order} handleOrderDesc={handleOrderDesc} handleOrderAsc={handleOrderAsc} />
                  </div>
                </th>
                <th className="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                  <div className='flex items-center'>
                    <div className="font-semibold text-left"><Trans>Tax</Trans></div>
                    <OrderArrowButtons orderBy='tax' orderBySelected={orderBy} orderSelected={order} handleOrderDesc={handleOrderDesc} handleOrderAsc={handleOrderAsc} />
                  </div>
                </th>
                <th className="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                  <div className='flex items-center'>
                    <div className="font-semibold text-left"><Trans>Stock</Trans></div>
                    <OrderArrowButtons orderBy='stock' orderBySelected={orderBy} orderSelected={order} handleOrderDesc={handleOrderDesc} handleOrderAsc={handleOrderAsc} />
                  </div>
                </th>
              </tr>
            </thead>
            {/* Table body */}
            <tbody className="text-sm divide-y divide-slate-200">
              {
                list.map(product => {
                  return (
                    <Products
                      key={product.id}
                      id={product.id}
                      title={product.title}
                      price={product.price}
                      tax={product.tax}
                      formattedPrice={product.formattedPrice}
                      taxLabel={product.taxLabel}
                      stock={product.stock}
                    />
                  )
                })
              }
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default ProductsTable;
